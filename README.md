# OSCP NOTES

> Shlok Yadav

These are my OSCP notes and resources.

 - [Enumeration](Notes/Enumeration.md)

 - [Linux Privilege Escalation](Notes/Linux-Privilege-Escalation.md)

 - [Windows Privilege Escalation](Notes/Windows-Privilege-Escalation.md)

 - File Transfers
	- [Windows File Transfers](Notes/Windows-File-Transfers.md)
	- [Linux File Transfers](Notes/Linux-File-Transfers.md)


 - [Powershell](Notes/Powershell.md)

 - [Port Forwarding and Tunneling](Notes/Port-Forwarding-Tunneling.md)

 - [Active Directory](Notes/Active-Directory.md)