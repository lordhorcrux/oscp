# NetBIOS Enumeration

> Step by Step NetBIOS Enumeration

- NetBIOS Names : Port 137/UDP or 137/TCP

- Session Service : Port 139/TCP

- Connectionless Communication : Port 138/UDP

Enumerating a NetBIOS service you can obtain the names the server is using and the MAC address of the server.

```
nmblookup -A <IP>
nbtscan <IP>/30
sudo nmap -sU -sV -T4 --script nbstat.nse -p137 -Pn -n <IP>
```

Scan for NetBIOS nameservers:

```
nbtscan -r 192.168.0.1/24
```