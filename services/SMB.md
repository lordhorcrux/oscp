# SMB Enumeration

> Step by Step SMB Enumeration

**SMB Version Enumeration:**

[Script Link](scripts/smbver.sh)

**Nmap SMB Scripts:**

```
ls -1 /usr/share/nmap/scripts/smb*
```

**SMB OS Discovery:**

```
nmap -v -p 139, 445 --script=smb-os-discovery 10.11.1.227
```

**Obtain Information:**

```
enum4linux -a [-u "<username>" -p "<passwd>"] <IP>
nmap --script "safe or smb-enum-*" -p 445 <IP>
```

**GUI Connection from Linux:**

```
xdg-open smb://cascade.htb/
```

In file browser window (nautilus, thunar, etc):

```
smb://friendzone.htb/general/
```

**List Shared Folders**

```
smbclient --no-pass -L //<IP>
smbmap -H <IP> [-P <PORT>]
crackmapexec smb <IP> -u '' -p '' --shares
```

**Connect to a Shared Folder**

```
smbclient --no-pass //<IP>/<Folder>
smbmap [-u "username" -p "password"] -R [Folder] -H <IP> [-P <PORT>] # Recursive list
smbmap [-u "username" -p "password"] -r [Folder] -H <IP> [-P <PORT>] # Non-Recursive list
```

**Manually enumerate windows shares and connect to them**

```
smbclient -U '%' -N \\\\<IP>\\<SHARE>
```

Note - To enumerate the shares manually you might want to look for responses like NT_STATUS_ACCESS_DENIED and NT_STATUS_BAD_NETWORK_NAME, when using a valid session (e.g. null session or valid credentials). These may indicate whether the share exists and you do not have access to it or the share does not exist at all.

**Mount a Shared Folder**

```
mount -t cifs //x.x.x.x/share /mnt/share
mount -t cifs -o "username=user,password=password" //x.x.x.x/share /mnt/share
```

**Download Files**

Search and Download that file in /usr/share/smbmap

```
sudo smbmap -R Folder -H <IP> -A <FileName> -q
```

All Files Recursively:

```
smbget -R smb://fileserver/directory
```

```
smbclient //<IP>/<share>
mask ""
recurse
prompt
mget *
```

Uploading File To Share

```
smbmap -H <IP> --upload SRC DST
```