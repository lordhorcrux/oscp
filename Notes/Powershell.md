# Powershell Basics

> Offensive Powershell

Cmdlets are powershell commands. Get-Command and Get-Help are your best friends!

**Get-Help**

`Get-Help Get-Command -Examples` - Using `Get-Help` command

**Get-Command**

`Get-Command Verb-*` or `Get-Command *-Noun`

Get-Command gets all the cmdlets installed on the current device.

**Object Manipulation**

`Verb-Noun | Get-Member`

Usage: `Get-Command | Get-Member -MemberType Method`

**Filter Objects**

`Verb-Noun | Where-Object -Property PropertyName -operator Value`

`Verb-Noun | Where-Object {$_.PropertyName -operator Value}`

Where -operator is a list of the of the following operators.

 Contains - If any item in the property value is an exact match for the specified value

 EQ - If the property value is the same as the specified value.

 GT - If the property value is greater than the specified value

Usage: `Get-Service | Where-Object -Property Status -eq Stopped`

**Sort Objects**

`Verb-Noun | Sort-Object`

## Offensive Powershell

#### ActiveDirectory

**Importing Modules**

`Import-Module Module`

`. .\Module.ps1`

**Get-ADDomain**

`Get-ADDomain | Select-Object NetBIOSName, DNSRoot, InfrastructureMaster`

**Get-ADForest**

`Get-ADForest | Select-Object Domains`

**Get-ADTrust**

`Get-ADTrust -Filter * | Select-Object Direction,Source,Target`

#### Powerview

[Github Link](https://github.com/PowerShellMafia/PowerSploit/blob/master/Recon/PowerView.ps1)

**Get-NetDomain**

`Get-NetDomain`

**Get-NetDomainController**

`Get-NetDomainController`

**Get-NetForest**

`Get-NetForest`

**Get-NetDomainTrust**

`Get-NetDomainTrust`