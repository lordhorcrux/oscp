# Enumeration

> Step by Step Enumeration Methodology

### Nmap Scan

```
sudo nmap -sSCV -A -T4 -p- 192.168.101.123 -oA output.txt --min-rate=5000
```

### DNS Records

```
host www.megacorpone.com
```

MX and TXT records:

```
host -t mx www.megacorpone.com
host -t txt www.megacorpone.com
```

Name Servers:

```
host -t ns megacorpone.com | cut -d " " -f 4
```

DNS Zone Transfer:

```
host -l <domain-name> <dns-name-server>
dnsrecon -d <domain-name> -t axfr
dnsenum <domain-name>
```

### Masscan for oscp

```
sudo masscan -p80 10.11.1.0/24 --rate=2000 -e tun0 --router-ip 10.11.0.1
```

### NetBIOS Enumeration

[Link](services/Netbios.md)

### SMB Enumeration

[Link](services/SMB.md)

### NFS Enumeration

[Link](services/NFS.md)

### SMTP Enumeration

You can verify that an user exists or not.

[Script Link](scripts/smtpvrfy.py)

```
smtpvrfy.py <IP> <username>
```

User Enumeration:

```
smtp-user-enum -M VRFY -U /root/sectools/SecLists/Usernames/Names/names.txt -t 192.168.1.103
```

### SNMP Enumeration

[Link](services/SNMP.md)
